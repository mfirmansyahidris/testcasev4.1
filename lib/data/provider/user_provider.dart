import 'package:majootestcase/data/models/user.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class UserProvider {
  late Database db;
  late String path;

  static const String _tableName = "user";
  static const String email = "email";
  static const String userName = "userName";
  static const String password = "password";

  Future<void> open() async {
    String databasesPath = await getDatabasesPath();
    path = join(databasesPath, "movie.db");
    db = await openDatabase(path, version: 1, onCreate: (db, version) async {
      await db.execute('''
      create table $_tableName ( 
        $email text primary key, 
        $userName text not null,
        $password text not null)
      ''');
    });
  }

  Future<int> insert(User user) async {
    final result = await db.insert(_tableName, user.toMap(), conflictAlgorithm: ConflictAlgorithm.fail);
    return result;
  }

  Future<User?> getUser(User user) async {
    List<Map> maps = await db.query(_tableName,
        columns: [email, userName],
        where: '$email = ? AND $password = ?',
        whereArgs: [user.email, user.password]
    );
    if (maps.length > 0) {
      return User.fromMap(maps.first);
    }
    return null;
  }

  Future close() async => db.close();
}
