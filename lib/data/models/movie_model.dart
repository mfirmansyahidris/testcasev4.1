class MovieModel {
  int? page;
  List<MovieData>? results;
  int? totalPages;
  int? totalResults;
  bool isSuccess = true;
  String? responseMessage;

  MovieModel({this.page, this.results, this.totalPages, this.totalResults, this.isSuccess = true, this.responseMessage});

  MovieModel.fromJson(Map<String, dynamic> json) {
    page = json['page'];
    if (json['results'] != null) {
      results = <MovieData>[];
      json['results'].forEach((v) {
        results!.add(new MovieData.fromJson(v));
      });
    }
    totalPages = json['total_pages'];
    totalResults = json['total_results'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['page'] = this.page;
    if (this.results != null) {
      data['results'] = this.results!.map((v) => v.toJson()).toList();
    }
    data['total_pages'] = this.totalPages;
    data['total_results'] = this.totalResults;
    return data;
  }
}

class MovieData {
  bool? adult;
  String? backdropPath;
  String? title;
  List<int>? genreIds;
  String? originalLanguage;
  String? originalTitle;
  String? posterPath;
  bool? video;
  double? voteAverage;
  String? overview;
  String? releaseDate;
  int? voteCount;
  int? id;
  double? popularity;
  String? mediaType;
  String? originalName;
  String? originCountry;
  String? firstAirDate;
  String? name;

  MovieData(
      {this.adult,
        this.backdropPath,
        this.title,
        this.genreIds,
        this.originalLanguage,
        this.originalTitle,
        this.posterPath,
        this.video,
        this.voteAverage,
        this.overview,
        this.releaseDate,
        this.voteCount,
        this.id,
        this.popularity,
        this.mediaType,
        this.originalName,
        this.originCountry,
        this.firstAirDate,
        this.name});

  MovieData.fromJson(Map<String, dynamic> json) {
    adult = json['adult'];
    backdropPath = json['backdrop_path'];
    title = json['title'];
    genreIds = json['genre_ids'].cast<int>();
    originalLanguage = json['original_language'];
    originalTitle = json['original_title'];
    posterPath = json['poster_path'];
    video = json['video'];
    voteAverage = json['vote_average'];
    overview = json['overview'];
    releaseDate = json['release_date'];
    voteCount = json['vote_count'];
    id = json['id'];
    popularity = json['popularity'];
    mediaType = json['media_type'];
    originalName = json['original_name'];
    originCountry = json['origin_country'].toString();
    firstAirDate = json['first_air_date'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['adult'] = this.adult;
    data['backdrop_path'] = this.backdropPath;
    data['title'] = this.title;
    data['genre_ids'] = this.genreIds;
    data['original_language'] = this.originalLanguage;
    data['original_title'] = this.originalTitle;
    data['poster_path'] = this.posterPath;
    data['video'] = this.video;
    data['vote_average'] = this.voteAverage;
    data['overview'] = this.overview;
    data['release_date'] = this.releaseDate;
    data['vote_count'] = this.voteCount;
    data['id'] = this.id;
    data['popularity'] = this.popularity;
    data['media_type'] = this.mediaType;
    data['original_name'] = this.originalName;
    data['origin_country'] = this.originCountry;
    data['first_air_date'] = this.firstAirDate;
    data['name'] = this.name;
    return data;
  }
}
