import 'package:majootestcase/data/provider/user_provider.dart';

class User{
  String? email;
  String? userName;
  String? password;

  User({this.email, this.userName, this.password});

  User.fromJson(Map<String, dynamic> json)
      : email = json['email'],
        password = json['password'],
        userName = json['username'];

  Map<String, dynamic> toJson() => {
    'email': email,
    'password': password,
    'username' : userName
  };

  Map<String, Object?> toMap() {
    var map = <String, Object?>{
      UserProvider.email: email,
      UserProvider.userName: userName,
      UserProvider.password: password,
    };
    return map;
  }

  User.fromMap(Map<dynamic, Object?> map) {
    email = map[UserProvider.email].toString();
    userName = map[UserProvider.userName].toString();
    password = map[UserProvider.password].toString();
  }
}