import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:majootestcase/ui/app_route.dart';
import 'package:flutter/material.dart';
import 'package:oktoast/oktoast.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return OKToast(
      child: ScreenUtilInit(
        designSize: const Size(375, 667),
        builder: () => MaterialApp(
          title: 'Flutter Demo',
          theme: ThemeData(
            primarySwatch: Colors.blue,
            visualDensity: VisualDensity.adaptivePlatformDensity,
          ),
          builder: (BuildContext context, Widget? child) {
            final MediaQueryData data = MediaQuery.of(context);
            return MediaQuery(
              data: data.copyWith(
                textScaleFactor: 1,
                alwaysUse24HourFormat: true,
              ),
              child: child!,
            );
          },
          onGenerateRoute: (RouteSettings settings) {
            final routes = AppRoute.getRoutes(settings: settings);
            final WidgetBuilder? builder = routes[settings.name];
            return MaterialPageRoute(
              builder: (context) => builder!(context),
              settings: settings,
            );
          },
          initialRoute: AppRoute.mainPage,
        ),
      ),
    );
  }
}

