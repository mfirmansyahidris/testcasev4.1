import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/data/models/user.dart';
import 'package:majootestcase/data/provider/user_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'registration_bloc_state.dart';

class RegistrationBlocCubit extends Cubit<RegistrationBlocState> {
  RegistrationBlocCubit() : super(RegistrationBlocInitialState());

  void registration(User user) async{
    emit(RegistrationBlocLoadingState());
    try{
      final userProvider = UserProvider();
      await userProvider.open();
      await userProvider.insert(user);
      await userProvider.close();
      SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
      await sharedPreferences.setBool("is_logged_in",true);
      String data = user.toJson().toString();
      sharedPreferences.setString("user_value", data);
      emit(RegistrationBlocSuccessState());
    }catch(e){
      emit(RegistrationBlocErrorState("Error: Email telah didaftarkan sebelumnya"));
    }
  }
}