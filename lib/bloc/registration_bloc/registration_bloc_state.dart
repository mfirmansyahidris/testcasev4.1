part of 'registration_bloc_cubit.dart';

abstract class RegistrationBlocState extends Equatable {
  const RegistrationBlocState();

  @override
    List<Object> get props => [];
}

class RegistrationBlocInitialState extends RegistrationBlocState { }

class RegistrationBlocLoadingState extends RegistrationBlocState { }

class RegistrationBlocSuccessState extends RegistrationBlocState { }

class RegistrationBlocErrorState extends RegistrationBlocState {
    final String errorMessage;

    RegistrationBlocErrorState(this.errorMessage);
}
