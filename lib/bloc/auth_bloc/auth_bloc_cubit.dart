import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/data/models/user.dart';
import 'package:majootestcase/data/provider/user_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'auth_bloc_state.dart';

class AuthBlocCubit extends Cubit<AuthBlocState> {
  AuthBlocCubit() : super(AuthBlocInitialState());

  void fetchHistoryLogin() async{
    emit(AuthBlocInitialState());
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    bool isLoggedIn = sharedPreferences.getBool("is_logged_in") ?? false;
    if(isLoggedIn){
      emit(AuthBlocLoggedInState());
    }else{
      emit(AuthBlocLoginState());
    }
  }

  void loginUser(User user) async{
    emit(AuthBlocLoadingState());
    final userProvider = UserProvider();
    await userProvider.open();
    final result = await userProvider.getUser(user);
    if(result != null){
      SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
      await sharedPreferences.setBool("is_logged_in",true);
      String data = user.toJson().toString();
      sharedPreferences.setString("user_value", data);
      emit(AuthBlocLoggedInState());
    }else{
      emit(AuthBlocErrorState("Login gagal , periksa kembali inputan anda"));
    }
  }

  void logout() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    await sharedPreferences.setBool("is_logged_in", false);
    emit(AuthBlocLogoutState());
  }
}
