import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:majootestcase/data/models/movie_model.dart';
import 'package:majootestcase/ui/app_route.dart';
import 'package:majootestcase/ui/resources/resources.dart';
import 'package:majootestcase/utils/context.dart';

class HomeLoadedPage extends StatelessWidget {
  final List<MovieData> data;

  const HomeLoadedPage({Key? key, required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Palette.background,
        body: GridView.builder(
            padding: EdgeInsets.all(Dimens.space10),
            gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                maxCrossAxisExtent: context.widthInPercent(50),
                childAspectRatio: 0.55,
                crossAxisSpacing: Dimens.space10,
                mainAxisSpacing: Dimens.space10),
            itemCount: data.length,
            itemBuilder: (context, index) {
              return movieItemWidget(context, data[index]);
            }));
  }

  Widget movieItemWidget(BuildContext context, MovieData data) {
    return InkWell(
      onTap: (){
        context.goTo(AppRoute.detailPage, args: data);
      },
      borderRadius: BorderRadius.all(Radius.circular(Dimens.cornerRadius)),
      child: Card(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(Dimens.cornerRadius))),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(Dimens.cornerRadius),
          child: Column(
            children: [
              Container(
                constraints: BoxConstraints(
                    maxHeight: context.widthInPercent(180),
                    maxWidth: double.infinity),
                child: CachedNetworkImage(
                  fit: BoxFit.cover,
                  placeholder: (context, url) => const SizedBox(),
                  imageUrl:
                      "https://image.tmdb.org/t/p/w500/${data.posterPath ?? ""}",
                ),
              ),
              Expanded(
                child: Container(
                  alignment: Alignment.center,
                  padding: EdgeInsets.symmetric(
                      vertical: Dimens.space8, horizontal: Dimens.space16),
                  child: Text(
                      data.originalTitle ?? data.title ?? data.name ?? "",
                      textAlign: TextAlign.center,
                      textDirection: TextDirection.ltr,
                    style: TextStyles.h6.copyWith(fontWeight: FontWeight.bold),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
