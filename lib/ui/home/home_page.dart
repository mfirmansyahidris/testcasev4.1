import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/ui/app_route.dart';
import 'package:majootestcase/ui/resources/palette.dart';
import 'package:majootestcase/utils/context.dart';
import '../../bloc/home_bloc/home_bloc_cubit.dart';
import 'home_loaded_page.dart';
import '../extra/loading.dart';
import '../extra/error_page.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  late HomeBlocCubit _homeBlocCubit;
  late AuthBlocCubit _authBlocCubit;

  @override
  void initState() {
    super.initState();
    _homeBlocCubit = BlocProvider.of(context);
    _authBlocCubit = BlocProvider.of(context);
    _homeBlocCubit.fetchingData();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<AuthBlocCubit, AuthBlocState>(
      bloc: _authBlocCubit,
      listener: (context, state){
        if(state is AuthBlocLogoutState){
          context.goToClearStack(AppRoute.loginPage);
        }
      },
      child: Scaffold(
        appBar: context.appBar(
          title: "Movi-E",
          isTransparent: false,
          actions: [
            TextButton(
              child: Text("Logout", style: TextStyle(color: Colors.white),),
              onPressed: (){
                context.dialogConfirm(
                  title: "Logout",
                  message: "Apakah anda yakin ingin Logout?",
                  onActionYes: (){
                    _authBlocCubit.logout();
                  }
                );
              },
            )
          ]
        ),
        body: BlocBuilder<HomeBlocCubit, HomeBlocState>(
            bloc: _homeBlocCubit,
            builder: (context, state) {
              if (state is HomeBlocLoadedState) {
                return HomeLoadedPage(data: state.data);
              } else if (state is HomeBlocLoadingState) {
                return LoadingIndicator();
              } else if (state is HomeBlocInitialState) {
                return Scaffold();
              } else if (state is HomeBlocErrorState) {
                return ErrorPage(
                  message: state.error,
                  textColor: Palette.black40,
                  retry: (){
                    _homeBlocCubit.fetchingData();
                  },
                );
              }

              return Center(
                  child: Text(kDebugMode ? "state not implemented $state" : ""));
            }),
      ),
    );
  }
}
