import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/registration_bloc/registration_bloc_cubit.dart';
import 'package:majootestcase/common/widget/button.dart';
import 'package:majootestcase/common/widget/spacer_v.dart';
import 'package:majootestcase/common/widget/text_form_field.dart';
import 'package:majootestcase/data/models/user.dart';
import 'package:majootestcase/ui/app_route.dart';
import 'package:majootestcase/ui/resources/resources.dart';
import 'package:majootestcase/utils/context.dart';
import 'package:majootestcase/utils/string.dart';



class RegistrationPage extends StatefulWidget {
  @override
  _RegistrationState createState() => _RegistrationState();
}

class _RegistrationState extends State<RegistrationPage> {
  final _emailController = TextController(initialValue: '');
  final _passwordController = TextController(initialValue: '');
  final _userNameController = TextController(initialValue: "");
  GlobalKey<FormState> formKey = new GlobalKey<FormState>();

  bool _isObscurePassword = false;

  late RegistrationBlocCubit _blocCubit;

  @override
  void initState() {
    _blocCubit = BlocProvider.of(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocListener<RegistrationBlocCubit, RegistrationBlocState>(
        bloc: _blocCubit,
        listener: (context, state) {
          if(state is RegistrationBlocSuccessState){
            "Register Berhasil".toToastSuccess();
            context.goTo(AppRoute.mainPage);
          }else if(state is RegistrationBlocErrorState){
            state.errorMessage.toToastError();
          }
        },
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.only(
                top: Dimens.space64,
                left: Dimens.space20,
                bottom: Dimens.space20,
                right: Dimens.space20
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  'Daftar',
                  style: TextStyles.h4.copyWith(
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text(
                  'Silahkan melakukan pendaftaran',
                  style: TextStyles.body1,
                ),
                SpacerV(value: Dimens.space10,),
                _form(),
                SpacerV(value: Dimens.space10,),
                Button(
                  title: "Register",
                  showShadow: false,
                  width: context.widthInPercent(100),
                  padding: EdgeInsets.all(Dimens.space16),
                  onPressed: _handleRegister,
                ),
                SpacerV(value: Dimens.space46,),
                _login(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _form() {
    return Form(
      key: formKey,
      child: Column(
        children: [
          CustomTextFormField(
            context: context,
            controller: _emailController,
            isEmail: true,
            hint: 'Example@123.com',
            label: 'Email',
            validator: (val) {
              final pattern = new RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');
              if (val != null)
                return pattern.hasMatch(val) ? null : 'Masukkan email yang valid';
            },
          ),
          CustomTextFormField(
            context: context,
            label: 'Password',
            hint: 'password',
            controller: _passwordController,
            isObscureText: _isObscurePassword,
            suffixIcon: IconButton(
              icon: Icon(
                _isObscurePassword
                    ? Icons.visibility_off_outlined
                    : Icons.visibility_outlined,
              ),
              onPressed: () {
                setState(() {
                  _isObscurePassword = !_isObscurePassword;
                });
              },
            ),
          ),
          CustomTextFormField(
            context: context,
            controller: _userNameController,
            hint: 'Majoo Super Apps,',
            label: 'User Name',
            validator: (val) {
              if (val == null) return 'Masukkan User Name';
              return null;
            },
          ),
        ],
      ),
    );
  }

  Widget _login() {
    return Align(
      alignment: Alignment.center,
      child: TextButton(
        onPressed: () async {
          context.back();
        },
        child: RichText(
          text: TextSpan(
              text: 'Sudah punya akun? ',
              style: TextStyle(color: Colors.black45),
              children: [
                TextSpan(
                  text: 'Login',
                ),
              ]),
        ),
      ),
    );
  }

  void _handleRegister() async {
    final _email = _emailController.value;
    final _password = _passwordController.value;
    final _userName = _userNameController.value;

    if(_email.isEmpty && _password.isEmpty && _userName.isEmpty){
      "Form tidak boleh kosong".toToastError();
      return;
    }
    if (formKey.currentState?.validate() == true) {
      User user = User(
        email: _email,
        password: _password,
        userName: _userName
      );
      _blocCubit.registration(user);
    }
  }
}
