import 'package:flutter_screenutil/flutter_screenutil.dart';

class Dimens {
  Dimens._();

  static double h1 = 22.sp;
  static double h2 = 22.sp;
  static double h3 = 20.sp;
  static double h4 = 18.sp;
  static double h5 = 20.sp;
  static double h6 = 16.sp;
  static double body1 = 14.sp;
  static double body2 = 12.sp;
  static double subtitle1 = 15.sp;
  static double subtitle2 = 14.sp;
  static double buttonS = 10.sp;
  static double button = 12.sp;
  static double buttonL = 14.sp;
  static double caption = 12.sp;
  static double overline = 10.sp;

  static double space2 = 2.w;
  static double space3 = 3.w;
  static double space4 = 4.w;
  static double space6 = 6.w;
  static double space8 = 8.w;
  static double space9 = 9.w;
  static double space10 = 10.w;
  static double space12 = 12.w;
  static double space16 = 16.w;
  static double space20 = 20.w;
  static double space24 = 24.w;
  static double space28 = 28.w;
  static double space30 = 30.w;
  static double space32 = 32.w;
  static double space36 = 36.w;
  static double space46 = 46.w;
  static double space50 = 50.w;
  static double space64 = 64.w;

  static double selectedIndicatorW = 43.w;
  static double selectedIndicatorSmallW = 28.w;
  static double heightAppbarHome = 65.w;
  static double tab = 38.w;
  static double menu = 72.w;
  static double menuContainer = 250.w;
  static double carousel = 167.w;
  static double newsHeight = 185.w;

  static double header = 200.w;
  static double minLabel = 100.w;
  static double bottomBar = 80.w;
  static double profilePicture = 64.w;
  static double birthdayCalendar = 120.w;
  static double imageThumbnail = 60.w;

  static double buttonH = 40.w;
  static double imageW = 110.w;
  static double communityTab = 90.w;
  static double slider = 180.w;

  static double featuredWidth = 120.w;
  static double featuredHeight = 150.w;
  static double featuredList = 220.w;

  static const double cornerRadius = 5;
  static const double cornerRadiusL = 15;
  static const double cornerRadiusXL = 30;

  static double cardCarouselHeight = 320.w;
  static double cardCarouselWidth = 300.w;
  static double addCartHeight = 140.w;
}
