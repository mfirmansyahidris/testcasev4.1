import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/common/widget/button.dart';
import 'package:majootestcase/common/widget/spacer_v.dart';
import 'package:majootestcase/common/widget/text_form_field.dart';
import 'package:majootestcase/data/models/user.dart';
import 'package:majootestcase/ui/app_route.dart';
import 'package:majootestcase/utils/context.dart';
import 'package:majootestcase/utils/string.dart';

import '../resources/resources.dart';



class LoginPage extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<LoginPage> {
  final _emailController = TextController(initialValue: '');
  final _passwordController = TextController(initialValue: '');
  GlobalKey<FormState> formKey = new GlobalKey<FormState>();

  bool _isObscurePassword = true;

  late AuthBlocCubit _authBlocCubit;

  @override
  void initState() {
    super.initState();

    _authBlocCubit = BlocProvider.of(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocListener<AuthBlocCubit, AuthBlocState>(
        bloc: _authBlocCubit,
        listener: (context, state) {
          if(state is AuthBlocLoggedInState){
            "Login Berhasil".toToastSuccess();
            context.goToClearStack(AppRoute.homePage);
          }else if(state is AuthBlocErrorState){
            state.error.toToastError();
          }
        },
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.only(
                top: Dimens.space64,
                left: Dimens.space20,
                bottom: Dimens.space20,
                right: Dimens.space20
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  'Selamat Datang',
                  style: TextStyles.h4.copyWith(
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text(
                  'Silahkan login terlebih dahulu',
                  style: TextStyles.body1,
                ),
                SpacerV(value: Dimens.space10,),
                _form(),
                SpacerV(value: Dimens.space10,),
                Button(
                  title: "Login",
                  showShadow: false,
                  width: context.widthInPercent(100),
                  padding: EdgeInsets.all(Dimens.space16),
                  onPressed: handleLogin,
                ),
                SpacerV(value: Dimens.space46,),
                _register(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _form() {
    return Form(
      key: formKey,
      child: Column(
        children: [
          CustomTextFormField(
            context: context,
            controller: _emailController,
            isEmail: true,
            hint: 'Example@123.com',
            label: 'Email',
            validator: (val) {
              final pattern = new RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');
              if (val != null){
                return pattern.hasMatch(val) ? null : 'Masukkan email yang valid';
              }
              return null;
            },
          ),
          CustomTextFormField(
            context: context,
            label: 'Password',
            hint: 'password',
            controller: _passwordController,
            isObscureText: _isObscurePassword,
            suffixIcon: IconButton(
              icon: Icon(
                _isObscurePassword
                    ? Icons.visibility_off_outlined
                    : Icons.visibility_outlined,
              ),
              onPressed: () {
                setState(() {
                  _isObscurePassword = !_isObscurePassword;
                });
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget _register() {
    return Align(
      alignment: Alignment.center,
      child: TextButton(
        onPressed: () async {
          context.goTo(AppRoute.registrationPage);
        },
        child: RichText(
          text: TextSpan(
              text: 'Belum punya akun? ',
              style: TextStyle(color: Colors.black45),
              children: [
                TextSpan(
                  text: 'Daftar',
                ),
              ]),
        ),
      ),
    );
  }

  void handleLogin() async {
    final _email = _emailController.value;
    final _password = _passwordController.value;

    if(_email.isEmpty && _password.isEmpty){
      "Form tidak boleh kosong".toToastError();
      return;
    }

    if (formKey.currentState?.validate() == true) {
      User user = User(
          email: _email,
          password: _password,
      );
      _authBlocCubit.loginUser(user);
    }
  }
}
