import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:majootestcase/common/widget/spacer_h.dart';
import 'package:majootestcase/common/widget/spacer_v.dart';
import 'package:majootestcase/data/models/movie_model.dart';
import 'package:majootestcase/ui/resources/resources.dart';
import 'package:majootestcase/utils/context.dart';

class DetailPage extends StatefulWidget {
  final MovieData movieData;
  const DetailPage({Key? key, required this.movieData}) : super(key: key);

  @override
  State<DetailPage> createState() => _DetailPageState();
}

class _DetailPageState extends State<DetailPage> {
  String title = "";
  String year = "";

  @override
  void initState() {
    super.initState();
    title = widget.movieData.originalTitle ?? widget.movieData.title ?? widget.movieData.name ?? "";
    final date = DateTime.tryParse(widget.movieData.releaseDate ?? widget.movieData.firstAirDate ?? "");
    year = date?.year.toString() ?? "-";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      body: Stack(
        children: [
          _header(),
          CustomScrollView(
            slivers: <Widget>[
              _appBarView(context),
              _contentSection(),
            ],
          ),
        ],
      ),
    );
  }

  Widget _header(){
    return Container(
      height: context.heightInPercent(35),
      width: double.infinity,
      child: CachedNetworkImage(
        fit: BoxFit.cover,
        placeholder: (context, url) => CircularProgressIndicator(),
        imageUrl: "https://image.tmdb.org/t/p/w500/${widget.movieData.posterPath ?? ""}",
      ),
    );
  }

  Widget _appBarView(BuildContext context) {
    return SliverAppBar(
      elevation: 0,
      title: Text(title),
      centerTitle: true,
      backgroundColor: Colors.transparent,
      expandedHeight: context.heightInPercent(35) - 75,
      snap: false,
      pinned: false,
      flexibleSpace: FlexibleSpaceBar(
        background: Container(
          decoration: BoxDecoration(color: Colors.transparent),
        ),
      ),
    );
  }

  Widget _contentSection() {
    return SliverList(
      delegate: SliverChildListDelegate(
        [
          Container(
            width: double.infinity,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(20),
                topRight: Radius.circular(20),
              ),
            ),
            padding: EdgeInsets.all(Dimens.space20),
            child: Column(
              children: [
                _itemInfo(label: "Title", value: title),
                SpacerV(value: Dimens.space10,),
                _itemInfo(label: "Overview", value: widget.movieData.overview ?? "-"),
                SpacerV(value: Dimens.space10,),
                _itemRating(value: widget.movieData.voteAverage.toString()),
                SpacerV(value: Dimens.space10,),
                _itemInfo(label: "Popularity", value: widget.movieData.popularity.toString()),
                SpacerV(value: Dimens.space10,),
                _itemInfo(label: "Tahun", value: year),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _itemInfo({required String label, required String value}){
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Expanded(
          child: Text(
            label,
            style: TextStyles.body1.copyWith(fontWeight: FontWeight.bold),
          ),
          flex: 1,
        ),
        SpacerH(value: Dimens.space16,),
        Expanded(
          child: Text(
            value,
            style: TextStyles.body1,
          ),
          flex: 3,
        )
      ],
    );
  }

  Widget _itemRating({required String value}){
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Expanded(
          child: Text(
            "Rating",
            style: TextStyles.body1.copyWith(fontWeight: FontWeight.bold),
          ),
          flex: 1,
        ),
        SpacerH(value: Dimens.space16,),
        Expanded(
          child: Row(
            children: [
              Icon(Icons.star, color: Palette.yellow,),
              SpacerH(value: Dimens.space16,),
              Text(
                value,
                style: TextStyles.body1,
              )
            ],
          ),
          flex: 3,
        )
      ],
    );
  }
}
