import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/bloc/home_bloc/home_bloc_cubit.dart';
import 'package:majootestcase/bloc/registration_bloc/registration_bloc_cubit.dart';
import 'package:majootestcase/data/models/movie_model.dart';
import 'package:majootestcase/ui/detail/detail_page.dart';
import 'package:majootestcase/ui/home/home_page.dart';
import 'package:majootestcase/ui/login/login_page.dart';
import 'package:majootestcase/ui/main_page.dart';
import 'package:majootestcase/ui/registration/registration_page.dart';

class AppRoute {
  AppRoute._();

  //define page route name
  static const String loginPage = "loginPage";
  static const String mainPage = "mainPage";
  static const String registrationPage = "registerPage";
  static const String homePage = "homePage";
  static const String detailPage = "detailPage";

  //define page route
  static Map<String, WidgetBuilder> getRoutes({RouteSettings? settings}) => {
      loginPage: (_) {
        return BlocProvider(
          create: (context) => AuthBlocCubit(),
          child: LoginPage()
        );
      },
      mainPage: (_) {
        return BlocProvider(
          create: (context) => AuthBlocCubit(),
          child: MainPage()
        );
      },
      registrationPage: (_) {
        return BlocProvider(
          create: (context) => RegistrationBlocCubit(),
          child: RegistrationPage(),
        );
      },
      homePage: (_) {
        return MultiBlocProvider(
          providers: [
            BlocProvider(create: (context) => HomeBlocCubit(),),
            BlocProvider(create: (context) => AuthBlocCubit(),)
          ],
          child: HomePage(),
        );
      },
      detailPage: (_){
        MovieData movieData = MovieData();
        if(settings != null){
          if(settings.arguments != null){
            movieData = settings.arguments! as MovieData;
          }
        }
        return DetailPage(movieData: movieData,);
      }
    };
}
