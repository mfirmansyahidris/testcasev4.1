import 'package:flutter/material.dart';
import 'package:majootestcase/utils/context.dart';

import '../resources/resources.dart';

class ErrorPage extends StatelessWidget {
  final String message;
  final VoidCallback? retry;
  final Color? textColor;
  final Widget? retryButton;

  const ErrorPage(
      {Key? key,
      this.retryButton,
      this.message = "",
      this.retry,
      this.textColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Icon(
              Icons.network_check,
              color: textColor,
              size: context.widthInPercent(30),
            ),
            Text(
              message,
              style: TextStyles.subtitle1.copyWith(color: textColor),
            ),
            retry != null
                ? Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(
                        height: Dimens.space16,
                      ),
                      retryButton ??
                          IconButton(
                            color: Palette.primary,
                            onPressed: () {
                              if (retry != null) retry?.call();
                            },
                            icon: Icon(
                              Icons.refresh_sharp,
                              size: Dimens.space30,
                            ),
                          ),
                    ],
                  )
                : SizedBox()
          ],
        ),
      ),
    );
  }
}
