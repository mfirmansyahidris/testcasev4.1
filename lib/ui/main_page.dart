import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/ui/app_route.dart';
import 'package:majootestcase/utils/context.dart';

class MainPage extends StatefulWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  State<MainPage> createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  late AuthBlocCubit _authBlocCubit;

  @override
  void initState() {
    super.initState();
    _authBlocCubit = BlocProvider.of(context);
    _authBlocCubit.fetchHistoryLogin();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<AuthBlocCubit, AuthBlocState>(
      bloc: _authBlocCubit,
      listener: (context, state) {
        if (state is AuthBlocLoginState) {
          context.goToClearStack(AppRoute.loginPage);
        } else if (state is AuthBlocLoggedInState) {
          context.goToClearStack(AppRoute.homePage);
        }
      },
      child: Scaffold(
        body: SafeArea(
          child: Center(
            child: Text("Please Wait..."),
          ),
        ),
      ),
    );
  }
}

