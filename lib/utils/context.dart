import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:majootestcase/common/widget/spacer_h.dart';
import 'package:majootestcase/common/widget/spacer_v.dart';
import 'package:majootestcase/ui/resources/resources.dart';

extension ContextExtensions on BuildContext {
  Future<dynamic> goTo(String routeName, {Object? args}) =>
      Navigator.pushNamed(this, routeName, arguments: args);

  Future<dynamic> goToReplace(String routeName, {Object? args}) =>
      Navigator.pushReplacementNamed(this, routeName, arguments: args);

  Future<dynamic> goToClearStack(String routeName, {Object? args}) =>
      Navigator.pushNamedAndRemoveUntil(
        this,
        routeName,
            (Route<dynamic> route) => false,
        arguments: args,
      );

  void back() => Navigator.pop(this);

  Future<void> dialogConfirm({
    required String title,
    required String message,
    Color? color,
    VoidCallback? onActionYes,
    VoidCallback? onActionNo,
    String? buttonPositive,
    String? buttonNegative
  }) async {
    showDialog(
      context: this,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Row(
            children: <Widget>[
              Icon(Icons.report, color: color ?? Palette.primary),
              SpacerH(value: Dimens.space8,),
              Expanded(
                child: Text(
                  title,
                  style: TextStyles.body1.copyWith(color: color ?? Palette.primary),
                ),
              ),
            ],
          ),
          content: Text(
            message,
            style: TextStyles.body1,
          ),
          contentPadding: EdgeInsets.symmetric(
            horizontal: Dimens.space30,
            vertical: Dimens.space24,
          ),
          actions: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                TextButton(
                  onPressed: () {
                    Navigator.pop(context);
                    onActionYes?.call();
                  },
                  child: Text(
                    buttonPositive ?? "Yes",
                    style: TextStyles.body1.copyWith(
                      color: color ?? Palette.primary,
                    ),
                  ),
                ),
                const SpacerH(),
                TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                    onActionNo?.call();
                  },
                  child: Text(
                    buttonNegative ?? "Cancel",
                    style: TextStyles.body1.copyWith(color: color ?? Palette.primary),
                  ),
                ),
              ],
            )
          ],
        );
      },
    );
  }

  PreferredSize appBar({
    String? title,
    bool isTransparent = false,
    List<Widget>? actions,
  }) =>
      PreferredSize(
        preferredSize: const Size.fromHeight(kToolbarHeight),
        child: AppBar(
          elevation: 0.0,
          centerTitle: true,
          title: Text(
            title ?? "",
            style: TextStyles.h6.copyWith(
              color: Palette.white,
              fontWeight: FontWeight.w600,
            ),
          ),
          actions: actions,
          backgroundColor: isTransparent ? Colors.transparent : null,
          systemOverlayStyle: const SystemUiOverlayStyle(
            statusBarColor: Colors.transparent,
            statusBarIconBrightness: Brightness.dark,
            statusBarBrightness: Brightness.light,
          ),
        ),
      );

  double widthInPercent(double percent) {
    final toDouble = percent / 100;
    return MediaQuery.of(this).size.width * toDouble;
  }

  double heightInPercent(double percent) {
    final toDouble = percent / 100;
    return MediaQuery.of(this).size.height * toDouble;
  }
}
