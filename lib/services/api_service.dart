import 'package:dio/dio.dart';
import 'package:majootestcase/data/models/movie_model.dart';

import 'package:majootestcase/services/dio_config_service.dart' as dioConfig;

class ApiServices{

  Future<MovieModel> getMovieList() async {
    try {
      final  dio = await dioConfig.dio();
      final req = await dio?.get("");
      if(req?.statusCode == 200 || req?.statusCode == 201){
        return MovieModel.fromJson(req?.data);
      }else{
        return MovieModel(
          isSuccess: false,
          responseMessage: req?.data.message ?? "Unknown Error");
      }
    } on DioError catch (e){
      return MovieModel(isSuccess: false, responseMessage: "Network Error");
    }
    catch(e) {
      return MovieModel(
          isSuccess: false,
          responseMessage: e.toString()
      );
    }
  }
}